/*
	10. November 2022
*/

//Including libraries and declaring namespace
#include<iostream>
#include<cmath>

using namespace std;

int main() {

    double cathete1 = 0;
    double cathete2 = 0;
    double expected_hypotenuse_buffer = 0;
    double expected_hypotenuse = 0;
    double measured_hypotenuse = 0;
    string input_error = "Error: Invalid input";

    while (true) {
        printf ("%s \n", "Cathete 1 (in m):");

        while( !( cin >> cathete1 )) {
            string input1;
            cin.clear();	//you need to clear the flags before input
            getline( cin, input1 );	//read what was written. Since you probably don't need this, look into cin.ignore()
            printf ("%s \n", input_error.c_str());	//Printing the string defined at the beginning of main
        }

        printf ("%s \n", "Cathete 2 (in m):");

        while( !( cin >> cathete2 )) {
            string input2;
            cin.clear();
            getline( cin, input2);
            printf ("%s \n", input_error.c_str());
        }

        printf ("%s \n", "Measured hypotenuse (in m):");

        while( !( cin >> measured_hypotenuse )) {
            string input3;
            cin.clear();
            getline( cin, input3);
            printf ("%s \n", input_error.c_str());
        }

        expected_hypotenuse_buffer = pow(cathete1, 2) + pow(cathete2, 2);   //Calculating the predicted value of the hypotenuse
        expected_hypotenuse = sqrt(expected_hypotenuse_buffer);             //by using the pythagorean theorem
        printf ("\nHypotenuse: %4.3f \n\n", expected_hypotenuse);   //Printing expected value based on previously entered values

        double delta = 0;
        delta = expected_hypotenuse - measured_hypotenuse;      //Calculating difference (which is commonly refered to as Delta)
        printf ("%s \n", "(Delta refers to the value that the measured hypotenuse is too short.)");
        printf ("%s", "Delta:");
        printf ("%4.3f", delta);      //Printing delta with 3 a precision of 3 decimal places
        printf ("%s \n", "m");
    }

return 0;
}
